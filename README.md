# ReadOutFolderIntoCSV

### Use the script as follows.
1. Open a Powershell commandline.
2. In the Powershell terminal, Go to the location of the script. In other words, cd to the location of the script. For example. If the script is on the desktop of the "Common" user, so at ```C:\Users\Common\Desktop\printTree.ps1```, then type: ```cd C:\Users\Common\Desktop```.
3. Run the script: ```.\printTree.ps1 <location to list> <location to save csv to>``` example: ```.\printTree.ps1 C:\ C:\Users\Common\Desktop\myFiles.csv```
4. Let the script run. When it is unable to access a file, it will give an error but will continue with searching.
5. Your csv is now ready wit all the meta information of the files and zip-files. The layout is as follows
     ```
        - filename
        - file path (path of zip file in case of file inside zip file)
        - file date (date when file was zipped in case of file inside zip file)
        - size (original file in case of file inside zip file)
        - compressed size (same as size in case of normal file)
        - is file in side zip file (true or false)
        - location in zip file (file path in case of normal file)
     ```
