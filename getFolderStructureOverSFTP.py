import shutil
import pysftp


foldersOnSftp = None
filesOnSftp = None
activeUser = ""

def fileCallback(pathname):
    global activeUser
    global filesOnSftp
    #filesOnSftp.write(activeUser + ";" + pathname + ";" + pathname.split("/")[-1] + ";0\n")


def unknownCallback(pathname):
    global activeUser
    global filesOnSftp
    #filesOnSftp.write(activeUser + ";" + pathname + ";" + pathname.split("/")[-1] + ";1\n")


def dirCallback(pathname):
    global activeUser
    global foldersOnSftp
    print(activeUser + ";" + pathname + ";" + pathname.split("/")[-1])
    foldersOnSftp.write(activeUser + ";" + pathname + ";" + pathname.split("/")[-1] + "\n")


def readFromUser(server, username, password):
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    global activeUser
    activeUser = username

    with pysftp.Connection(server, username=username, password=password,
                           cnopts=cnopts) as sftp:
        sftp.walktree("/var/web/" + username + "/", fileCallback, dirCallback, unknownCallback)


if __name__ == '__main__':
    foldersOnSftp = open("./foldersOnSftp.csv", "w")
    foldersOnSftp.write("User;Path;Foldername\n")
    #filesOnSftp = open("./filesOnSftp.csv", "w")
    #filesOnSftp.write("User;Path;Filename;symLink\n")
    readFromUser("sftp.nrgfin.be", "vd5160", "dei4Zeic")
    readFromUser("sftp.nrgfin.be", "vd5024", "oe9eeMai")
    readFromUser("sftp.nrgfin.be", "vd5037", "ooNgohn4")
    readFromUser("sftp.nrgfin.be", "vd5095", "Weor7Eey")
    readFromUser("sftp.nrgfin.be", "vd5096", "Uaxooc9G")
    readFromUser("sftp.nrgfin.be", "vd5111", "aeziePh7")
    #readFromUser("sftp.nrgfin.be", "vd5183", "deeTh9ja")
    #readFromUser("sftp.nrgfin.be", "vd5290", "sur9eiZa")
    readFromUser("sftp.nrgfin.be", "vd5640", "Ier4aeNe")

    readFromUser("sftp.nrgfin.be", "vd5947", "atheSae9")
    readFromUser("sftp.nrgfin.be", "vd6355", "ooroRoi9")
    readFromUser("sftp.nrgfin.be", "vd6597", "eemaeQu3")
    readFromUser("sftp.nrgfin.be", "vd6598", "theiBuu3")
    readFromUser("sftp.nrgfin.be", "vd6599", "aePh4aew")
    readFromUser("sftp.nrgfin.be", "vd6753", "shohph3E")
    readFromUser("sftp.nrgfin.be", "vd6081", "quioch9G")
    readFromUser("sftp.nrgfin.be", "vd8005", "Shei4eir")

    readFromUser("sftp.nrgfin.be", "vd8100", "Ohd4tou7")
    readFromUser("sftp.nrgfin.be", "vd8103", "OokeZ9hu")
    readFromUser("sftp.nrgfin.be", "vd8329", "Du4eegh7")

    foldersOnSftp.close()