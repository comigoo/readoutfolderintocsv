import shutil
import pysftp
import os, sys
import configparser

filesOnSftp = None


def fileCallback(pathname):
    global activeUser
    global foldersOnSftp
    print(pathname.split("/")[-1] + ";" + pathname)
    filesOnSftp.write(pathname.split("/")[-1] + ";" + pathname + "\n")


def unknownCallback(pathname):
    pass


def dirCallback(pathname):
    pass


def readFolder(SFTPConnection, startpath, type):
    if type == "E26":
        SFTPConnection.walktree(startpath + "UTILTS/E26/", fileCallback, dirCallback, unknownCallback)
    elif type == "E31":
        SFTPConnection.walktree(startpath + "UTILTS/E31/", fileCallback, dirCallback, unknownCallback)
    elif type == "E32":
        SFTPConnection.walktree(startpath + "UTILTS/E32/", fileCallback, dirCallback, unknownCallback)
    elif type == "INVOIC":
        SFTPConnection.walktree(startpath + "INVOIC/", fileCallback, dirCallback, unknownCallback)


if __name__ == '__main__':

    configParser = configparser.RawConfigParser()
    configParser.read('config.cfg')

    keyAuthentication = configParser.getboolean('overal', 'key-authentication')
    serverName = configParser.get('overal', 'server')
    username = configParser.get('overal', 'username')
    password = configParser.get('overal', 'password')
    keyFile = configParser.get('overal', 'key-file')
    startMap = configParser.get('overal', 'start-map')
    typeToDownload = configParser.get('overal', 'type')
    outputfile = configParser.get('overal', 'outputfile')

    filesOnSftp = open(outputfile, "w")
    filesOnSftp.write("Filename;Path;CreationDate\n")

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None

    if keyAuthentication == "key":
        with pysftp.Connection(serverName, username=username, private_key=keyFile, cnopts=cnopts) as sftp:
            readFolder(sftp, startMap, typeToDownload)
    else :
        with pysftp.Connection(serverName, username=username, password=password, cnopts=cnopts) as sftp:
            readFolder(sftp, startMap, typeToDownload)

    filesOnSftp.close()