$stream = [System.IO.StreamWriter] ($args[1])
$fileId = 0

$checkDate = Get-Date -Format g

function getNextFileId {
    param()
    return $fileId + 1
}

function findUserInPath {
    param($thePath)
    $parts = $thePath.split("\\")
    foreach($part in $parts) {

        if ($part[0] -eq "v" -and $part[1] -eq "d") {
            return $part
        }
    }

    return "EMPTHY"
}

function isFileInInPutToNrgfinFolder {
    param($thePath)
    $parts = $thePath.split("\\")
    foreach($part in $parts) {

        if ($part -eq "InputToNrgfin") {
            return "true"
        }
    }

    return "false"
}

#Exit if the shell is using lower version of dotnet

$dotnetversion = [Environment]::Version
if(!($dotnetversion.Major -ge 4 -and $dotnetversion.Build -ge 30319)) {
    write-error "You are not having Microsoft DotNet Framework 4.5 installed. Script exiting"
    exit(1)
}

# Import dotnet libraries

[Void][Reflection.Assembly]::LoadWithPartialName('System.IO.Compression.FileSystem')

function Get-FriendlySize {
	param($Bytes)
    	$sizes='Bytes,KB,MB,GB,TB,PB,EB,ZB' -split ','
    	for($i=0; ($Bytes -ge 1kb) -and
        	($i -lt $sizes.Count); $i++) {$Bytes/=1kb}
    	$N=2; if($i -eq 0) {$N=0}
    	"{0:N$($N)} {1}" -f $Bytes, $sizes[$i]
}

function readOutZipFile {
    param($zipFile, $zipDate)
    if(Test-Path $zipFile) {
        $RawFiles = [IO.Compression.ZipFile]::OpenRead("$zipFile").Entries
        foreach($RawFile in $RawFiles) {
			if ($RawFile.Name -ne "") {
				$fileName = $RawFile.Name
				$filePath = $RawFile.FullName
				$size = $RawFile.Length
				$compressed = $RawFile.CompressedLength
                $process = Split-Path $zipFile
                $fileId = [guid]::NewGuid()
                $customerNumber = findUserInPath $zipFile
                $isFileInInputIntoNrgfin = isFileInInPutToNrgfinFolder $zipFile

				$stream.WriteLine("$fileId; $customerNumber; $process; $checkDate; $isFileInInputIntoNrgfin; $fileName; $zipFile; $zipDate; $size; $compressed; true; $filePath")
			}
        }
    } else {
        Write-Warning "$ZipFileInput File path not found"
    }
}

function readOutFolder {
	param( [string]$folderName, [int]$tabs)

	$folder = Get-ChildItem $folderName
	$tabsIndented = $tabs + 1

	foreach ($file in $folder) {
		$fullName = $folderName + "\" + $file

		if ((Get-Item $fullName) -is [System.IO.DirectoryInfo]) {
			readOutFolder "$fullName" $tabsIndented
		} else {
			$FileDate = $File.LastWriteTime
    			$CTDate2Str = $FileDate.ToString("dd/MM/yyyy")
			$fileSize = Get-FriendlySize $File.length
			$fileSizeRaw = $File.length
            $process = Split-Path $folderName -Leaf
            $fileId = [guid]::NewGuid()
            $customerNumber = findUserInPath $fullName
            $isFileInInputIntoNrgfin = isFileInInPutToNrgfinFolder $fullName

			$stream.WriteLine("$fileId; $customerNumber; $process; $checkDate; $isFileInInputIntoNrgfin; $file; $fullname; $CTDate2Str; $fileSizeRaw; $fileSizeRaw; false; $fullName")

			$extn = [IO.Path]::GetExtension($fullName)
			if ($extn -eq ".zip" ) {
				readOutZipFile $fullName $CTDate2Str
			}
		}
	}
}

readOutFolder $args[0] 0

$stream.close()
